package Game;

/**
 * Created by Stephan von Greiffenstern on 20.01.15.
 * Field. Here are several actions depending the field like moving in any direction
 */
public class GameField {
    /*
    30 | 41 | 42 | 43
    20 | 31 | 32 | 33
    10 | 11 | 12 | 13
    00 | 01 | 02 | 03
     */
    private NumberObj[][] field = new NumberObj[4][4];//[Zeile von unten][Spalte von links]
    private boolean running = true;
    private int score = 0;
    private final int laenge = 9;

    public boolean isRunning() {
        return running;
    }

    /**
     * moves all numbers right and puts them together if necessary.
     *
     * @param pLine only for a single line, enough anyway
     * @param pos   position
     * @return
     */
    private NumberObj[] moveRight(NumberObj[] pLine, int pos) {
        NumberObj[] line = pLine;
        if (pos > 0) {

            if (line[pos].getOwnValueInt() == 0) {
                line[pos].setOwnValue(line[pos - 1].getOwnValueInt());
                line[pos - 1].setOwnValue(0);
            } else {
                if (line[pos].getOwnValueInt() == line[pos - 1].getOwnValueInt() && !line[pos].isAlrDoubled() && !line[pos - 1].isAlrDoubled()) {
                    line[pos].doubleOwnValue();
                    score += line[pos].getOwnValueInt();
                    line[pos - 1].setOwnValue(0);
                    line[pos].setAlrDoubled();
                }
            }
            line = moveRight(line, pos - 1);
        }
        if (pos == 3 && !isValid(line)) {
            line = moveRight(line, pos);
        }
        return line;
    }

    /**
     * returns if the line is already done with moving
     *
     * @param pLine
     * @return
     */
    private boolean isValid(NumberObj[] pLine) {
        boolean b = true;
        boolean hatLeer = false;
        boolean hatNachfolger = false;
        int lastVal = 9;
        for (int i = 3; i >= 0; i--) {
            int t = pLine[i].getOwnValueInt();

            if (t == 0) {
                hatLeer = true;
            } else {
                if (t == lastVal) {
                    if (!pLine[i].isAlrDoubled() && !pLine[i + 1].isAlrDoubled()) {
                        b = false;
                    }
                } else {
                    lastVal = t;
                }
            }
            if (hatLeer && t > 0) {
                hatNachfolger = true;
            }
        }

        if (hatLeer && hatNachfolger) {
            b = false;
        }
        return b;
    }

    private boolean isFree(int pZeile, int pSpalte) {
        return field[pZeile][pSpalte].getOwnValueInt() == 0;
    }

    private boolean isFree(int pZeile, int pSpalte, NumberObj[][] pno) {
        return pno[pZeile][pSpalte].getOwnValueInt() == 0;
    }

    /**
     * returns if there is any free spot in the field
     * @return
     */
    private boolean hasFree() {
        boolean b = false;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {

                if (field[i][j].getOwnValueInt() == 0) {
                    b = true;
                }
            }
        }
        return b;
    }

    private NumberObj[][] rightR(NumberObj[][] pNO) {
        NumberObj[][] no = pNO;
        boolean valid = false;

        for (int i = 0; i < 4; i++) {
            NumberObj[] line = no[i];
            if (!isValid(line)) {
                valid = true; //if there is a line that is not done now, the alg is started
            }
        }

        if (valid) {
            for (int i = 0; i < 4; i++) {
                NumberObj[] line = no[i];
                line = moveRight(line, 3);
            }
            no=newNumber(no);
        }
        return no;
    }

    /**
     * move right
     */
    public void right() {
        field = rightR(field);
        endMove();
    }

    /**
     * move left
     */
    public void left() {
        NumberObj[][] temp = turn90right(field);
        temp = turn90right(temp);
        temp = rightR(temp);
        temp = turn90right(temp);
        field = turn90right(temp);
        endMove();
    }

    /**
     * move up
     */
    public void up() {
        NumberObj[][] temp = turn90right(field);
        temp = rightR(temp);
        temp = turn90right(temp);
        temp = turn90right(temp);
        field = turn90right(temp);
        endMove();
    }

    /**
     * move down
     */
    public void down() {
        NumberObj[][] temp = turn90right(field);
        temp = turn90right(temp);
        temp = turn90right(temp);
        temp = rightR(temp);
        field = turn90right(temp);
        endMove();
    }

    /**
     * adds a new random number (don't use this one!)
     */
    void newNumber() {

        if (hasFree()) {
            boolean b = true;
            int zeile = 99;
            int spalte = 99;

            while (b) {
                zeile = (int) Math.round(Math.random() * 3);
                spalte = (int) Math.round(Math.random() * 3);

                if (isFree(zeile, spalte)) {
                    b = false;
                }
            }
            field[zeile][spalte] = new NumberObj();
        } else {
            System.out.println("Leider verloren. ");
            System.out.println("Letzte Position:");
            printField();
            System.out.println();
            System.out.println("Punktzahl: " + score);
            score = 0;
            System.out.println("Neues Spiel startet automatisch.");
            running = false;
        }
    }

    /**
     * new Number will pe put randomly in the array
     * use this one
     * @param pno
     * @return
     */
    NumberObj[][] newNumber(NumberObj[][] pno) {

        if (hasFree()) {
            boolean b = true;
            int zeile = 99;
            int spalte = 99;

            while (b) {
                zeile = (int) Math.round(Math.random() * 3);
                spalte = (int) Math.round(Math.random() * 3);

                if (isFree(zeile, spalte, pno)) {
                    b = false;
                }
            }
            pno[zeile][spalte] = new NumberObj();
        } else {
            System.out.println("Leider verloren. ");
            System.out.println("Letzte Position:");
            printField();
            System.out.println();
            System.out.println("Punktzahl: " + score);
            score = 0;
            System.out.println("Neues Spiel startet automatisch.");
            running = false;
            System.out.println("Running auf false");
        }
        return pno;
    }

    /**
     * things that have to be done after every move
     */
    private void endMove() {
        //newNumber();
        //printField();

        //set alrDoubled false
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (field[i][j].isAlrDoubled()) {
                    field[i][j].setAlrDoubled();
                }
            }
        }
        running = zuegeMoeglich();
    }

    /**
     * Turn the array 90° right
     * @param pNO
     * @return
     */
    private NumberObj[][] turn90right(NumberObj[][] pNO) {

        NumberObj[][] ret = new NumberObj[4][4];

        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                ret[i][j] = pNO[4 - j - 1][i];
            }
        }
        return ret;
    }

    /**
     * print the field in the console
     */
    void printField() {
        for (int i = 0; i < 4; i++) { //i = Zeile
            for (int j = 0; j < 4; j++) { //j = Spalte
                String temp = field[i][j].getOwnValue();
                int temp_lae = laenge - temp.length();

                for (int k = 0; k < temp_lae / 2; k++) {
                    System.out.print(" ");
                }
                System.out.print(field[i][j].getOwnValue());
                for (int k = 0; k < temp_lae / 2; k++) {
                    System.out.print(" ");
                }
                System.out.print(" | ");

            }
            System.out.println("");
        }
        System.out.println();
    }

    /**
     * print score to console
     */
    public void printScore() {
        System.out.println("Score: " + score);
        System.out.println();
    }

    public NumberObj[][] getField() {
        return field;
    }

    public int getScore() {
        return score;
    }

    private boolean zuegeMoeglich() {
        for (int i = 0; i < 4; i++) {
            NumberObj[] no1 = new NumberObj[4];
            NumberObj[] no2 = new NumberObj[4];
            no1[0] = field[i][0];
            no1[1] = field[i][1];
            no1[2] = field[i][2];
            no1[3] = field[i][3];
            no2[0] = field[0][i];
            no2[1] = field[1][i];
            no2[2] = field[2][i];
            no2[3] = field[3][i];
            if (zuegeMoeglichTestLine(no1)) {
                return true;
            }
            if (zuegeMoeglichTestLine(no2)) {
                return true;
            }
        }

        return false;
    }

    private boolean zuegeMoeglichTestLine(NumberObj[] no) {
        int lastNumber = 0;

        for (int i = 0; i < 4; i++) {
            if (no[i].getOwnValueInt() == 0) {
                return true;
            }

            if (lastNumber == no[i].getOwnValueInt()) {
                return true;
            } else {
                lastNumber = no[i].getOwnValueInt();
            }
        }

        return false;
    }

    public GameField() {
        int[][] koord = new int[2][2];
        {
            boolean b = false;
            while (!b) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        koord[i][j] = (int) Math.round(Math.random() * 3);
                    }
                }
                b = !(koord[0][0] == koord[1][0] && koord[0][1] == koord[1][1]);
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i == koord[0][0] && j == koord[0][1]) {
                    field[i][j] = new NumberObj();
                } else if (i == koord[1][0] && j == koord[1][1]) {
                    field[i][j] = new NumberObj();
                } else {
                    field[i][j] = new NumberObj(0);
                }
            }
        }
        printField();
    }
}
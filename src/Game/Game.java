package Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Stephan von Greiffenstern on 19.01.15.
 * Controlclass. This can be dropped for a GUI or sth.
 */
public class Game {
    private GameField gf;


    private void start() {
        boolean running = true;

        while (running) {
            String zeile = "";
            try {
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
                System.out.print("Aktion: ");
                zeile = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            zeile = zeile.toLowerCase();

            switch (zeile) {
                case "u":
                case "e":
                case "up":
                    gf.up();
                    break;
                case "d":
                case "down":
                    gf.down();
                    break;
                case "l":
                case "s":
                case "left":
                    gf.left();
                    break;
                case "r":
                case "f":
                case "right":
                    gf.right();
                    break;
                case "end":
                    running = false;
                    break;
                case "score":
                    gf.printScore();
                    break;
            }

            if (!gf.isRunning()) {
                gf = new GameField();
            }

        }
        System.out.println("Bis zum nächsten Mal.");
    }


    public Game() {
        gf = new GameField();
        start();
    }
}
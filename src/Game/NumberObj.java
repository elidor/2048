package Game; /**
 * Created by Stephan von Greiffenstern on 19.01.15.
 */

import javafx.scene.image.Image;

public class NumberObj {
    private int ownValue;
    private boolean alrDoubled = false;
    private Image img;

    public boolean isAlrDoubled() {
        return alrDoubled;
    }

    public void setAlrDoubled() {
        alrDoubled = !alrDoubled;
    }

    public void doubleOwnValue() {
        setOwnValue(ownValue * 2);
    }

    public int getOwnValueInt() {
        return ownValue;
    }

    public Image getImg() {
        return img;
    }

    public String getOwnValue() {
        if(ownValue>0) {
            return "" + ownValue;
        } else {
            return "x";
        }
    }

    public void setOwnValue(int ownValue) {
        this.ownValue = ownValue;
        img = new Image("/GUI/img/" + ownValue + ".jpg");
    }

    public NumberObj() {
        int i = 0;
        while (i == 0) {
            i = (int) Math.round(Math.random() * 2);
        }
        setOwnValue(i * 2);
    }

    public NumberObj(int pI) {
        ownValue = 0;
        img = new Image("/GUI/img/0.jpg");
    }
}
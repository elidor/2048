package GUI;

import Game.GameField;
import Game.NumberObj;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Stephan von Greiffenstern on 17.02.15.
 */
public class Controller implements Initializable {
    @FXML
    private ImageView number00;
    @FXML
    private ImageView number01;
    @FXML
    private ImageView number02;
    @FXML
    private ImageView number03;
    @FXML
    private ImageView number10;
    @FXML
    private ImageView number11;
    @FXML
    private ImageView number12;
    @FXML
    private ImageView number13;
    @FXML
    private ImageView number20;
    @FXML
    private ImageView number21;
    @FXML
    private ImageView number22;
    @FXML
    private ImageView number23;
    @FXML
    private ImageView number30;
    @FXML
    private ImageView number31;
    @FXML
    private ImageView number32;
    @FXML
    private ImageView number33;
    @FXML
    private Label score;
    @FXML
    Label running;
    @FXML
    Label highscore;

    private ImageView[][] number;
    private int hs;

    private GameField gf = new GameField();

    @FXML
    private void handleButtonNew(ActionEvent e) {
        if (gf.getScore() > hs) {
            hs = gf.getScore();
        }
        gf = new GameField();
        paint();
    }

    @FXML
    private void handleKeyPressedSearchField(KeyEvent event) {
        if (event.getCode() == KeyCode.RIGHT) {
            gf.right();
        } else if (event.getCode() == KeyCode.LEFT) {
            gf.left();
        } else if (event.getCode() == KeyCode.UP) {
            gf.up();
        } else if (event.getCode() == KeyCode.DOWN) {
            gf.down();
        }
        paint();
    }

    private void paint() {
        NumberObj[][] no = gf.getField();
        highscore.setText("Highscore: " + hs);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                number[i][j].setImage(no[i][j].getImg());
            }
        }
        score.setText("" + gf.getScore());
        running.setText("");
        if (!gf.isRunning()) {
            System.out.println("VERLOREN!!!");
            running.setText("Leider verloren.");
            if (gf.getScore() > hs) {
                hs = gf.getScore();
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        hs = 0;
        System.out.println("test");
        number = new ImageView[4][4];
        number[0][0] = number00;
        number[0][1] = number01;
        number[0][2] = number02;
        number[0][3] = number03;

        number[1][0] = number10;
        number[1][1] = number11;
        number[1][2] = number12;
        number[1][3] = number13;

        number[2][0] = number20;
        number[2][1] = number21;
        number[2][2] = number22;
        number[2][3] = number23;

        number[3][0] = number30;
        number[3][1] = number31;
        number[3][2] = number32;
        number[3][3] = number33;
        paint();
    }
}
